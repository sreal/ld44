﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererManager : MonoBehaviour {
    public GameObject PlayerSprite;
    public GameObject DeadSprite;

    public GameObject BlankSprite;
    public GameObject HeartSprite;
    public GameObject MoneySprite;
    public GameObject GambleSprite;

    private GameObject playerActual;
    private GameObject[,] gridGameObjects;

    void Awake() {
        Camera.main.transform.position = new Vector3(4, 4, -10);
    }

    void DestroyGameGridObjects() {
        for (var j = 0; j < gridGameObjects.GetLength(0); j ++) {
            for (var i = 0; i < gridGameObjects.GetLength(1); i ++){
                Destroy(gridGameObjects[j, i]);
            }
        }
    }

    public void DrawGameGrid(GameGrid grid) {
        if (gridGameObjects == null) {
            gridGameObjects = new GameObject[grid.Width, grid.Height];
        }
        DestroyGameGridObjects();

        for (var j = 0; j < grid.Width; j ++) {
            for (var i = 0; i < grid.Height; i ++) {
                var cellType = grid.Cells[j, i];
                gridGameObjects[j, i] = DrawCell(cellType, j, i);
            }
        }
    }

    public void DrawPlayer(int position, bool isOutOfTime = false) {
        if (playerActual != null) {
            Destroy(playerActual);
        }

        var p = new Vector3(0, position, -1);
        if (isOutOfTime) {
            playerActual = Instantiate(DeadSprite, p, Quaternion.identity);
        } else {
            playerActual = Instantiate(PlayerSprite, p, Quaternion.identity);
        }
    }

    private GameObject DrawCell(CellObject type, int x, int y) {
        var sprite = BlankSprite;
        if (type == CellObject.Heart) {
            sprite = HeartSprite;
        } else
        if (type == CellObject.Money) {
            sprite = MoneySprite;
        } else
        if (type == CellObject.Gamble) {
            sprite = GambleSprite;
        }
        var p = new Vector3(x, y, 0);
        return Instantiate(sprite, p, Quaternion.identity);
    }
}
