﻿public enum Direction {
    None,
    Up,
    Down,
    Right
}
