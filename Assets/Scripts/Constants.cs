﻿public class Constants {

    public int Grid_Height = 8;
    public int Grid_Width = 3;

    public int Start_Hearts = 0;
    public int Start_Money = 0;

    public int Value_Hearts = 10;
    public int Value_Money = 100;

    public int TimeDecrement = 3;

    public int InitialStartTime = 10;
}
