﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGrid {

    public int Height { get; private set;}
    public int Width { get; private set; }

    public CellObject[,] Cells { get; private set; }

    public GameGrid(int height, int width) {
        Height = height;
        Width = width;
        Cells = Populate();
    }
    public CellObject GetCellObjectAt(int x, int y) {
        return Cells[x, y];
    }

    public void BlankCellObjectAt(int x, int y) {
        Cells[x, y] = CellObject.Blank;
    }

    public void ZeroCells() {
        for (var j = 0; j < Width; j ++) {
            for (var i = 0; i < Height; i ++) {
                Cells[j, i] = CellObject.Blank;
            }
        }
    }

    private CellObject[,] AddColumn(CellObject[,] cells, int colIndex) {
        var height = cells.GetLength(1);
        var arr = Enumerable.Repeat(0, height).ToArray();
        arr[0] = 1;
        if (Height > 1)
            arr[1] = 2;
        if (Height > 2)
            arr[2] = 3;
        arr = arr.OrderBy(x => UnityEngine.Random.value).ToArray();
        for (var i = 0; i < height; i ++) {
            var type = CellObject.Blank;
            switch( arr[i] ) {
            case 3:
                type = CellObject.Gamble;
                break;
            case 2:
                type = CellObject.Money;
                break;
            case 1:
                type = CellObject.Heart;
                break;
            }
            cells[colIndex, i] = type;
        }
        return cells;
    }

    private CellObject[,] Populate () {
        var grid = new CellObject[Width, Height];
        for (var j = 0; j < Width; j ++) {
            AddColumn(grid, j);
        }
        return grid;
    }

    public void Rotate() {
        for (var j = 1; j < Width; j ++) {
            // Shift cell left
            for (var i = 0; i < Height; i ++) {
                Cells[j-1,i] = Cells[j,i];
            }
            if (j == Width - 1) {
                // Add new last column
                Cells = AddColumn(Cells, j);
            }
        }
    }

}
