﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
    private Constants _const = new Constants();
    public Text TimeLabel;
    public Text HeartsLabel;
    public Text MoneyLabel;

    public Text TimeText;
    public Text HeartsText;
    public Text MoneyText;
    public Text GameOverText;

    public int Hearts;
    public int Money;
    public int Time;

    private int StartingTime;
    private bool isFirstTime = true;

    public void Reset(bool hard) {
        if (hard || isFirstTime) {
            StartingTime = _const.InitialStartTime;
        }
        if (StartingTime <= 0) {
            return;
        }

        if (isFirstTime) {
            isFirstTime = false;
        } else {
            StartingTime = StartingTime - UnityEngine.Random.Range(1, _const.TimeDecrement);
            if (StartingTime < 2) {
                StartingTime = 1;
            }
        }


        GameOverText.enabled = false;
        TimeText.enabled = true;
        HeartsText.enabled = true;
        MoneyText.enabled = true;

        TimeLabel.enabled = true;
        HeartsLabel.enabled = true;
        MoneyLabel.enabled = true;

        Hearts = _const.Start_Hearts;
        Money = _const.Start_Money;
        Time = StartingTime;
        UpdateUI();


    }

    public void ProcessMove() {
        Time--;
    }

    public bool IsOutOfTime() {
        return Time <= 0;
    }

    public void GameOver() {
        TimeText.enabled = false;
        HeartsText.enabled = false;
        MoneyText.enabled = false;
        TimeLabel.enabled = false;
        HeartsLabel.enabled = false;
        MoneyLabel.enabled = false;

        GameOverText.enabled = true;

        if (StartingTime <= 1) {
            GameOverText.text = String.Format(
                "You died a short life with ${0} and {1} friends.",
                Money, Hearts);
        } else {
            GameOverText.text = String.Format(
                "You died with ${0} and {1} friends.\n\nBuy life with [b]?",
                Money, Hearts);
        }
    }

    public void ProcessCell(CellObject type) {
        switch(type) {
        case CellObject.Heart:
            Hearts += UnityEngine.Random.Range(1, _const.Value_Hearts);
            break;
        case CellObject.Money:
            Money += UnityEngine.Random.Range(1, _const.Value_Money);
            break;
        case CellObject.Gamble:
            if (UnityEngine.Random.value < .3f)
                ProcessCell(CellObject.Heart);
            else
                if (UnityEngine.Random.value < .5f)
                    ProcessCell(CellObject.Money);
            break;
        }
        UpdateUI();
    }

    void UpdateUI() {
        HeartsText.text = Hearts.ToString();
        MoneyText.text = Money.ToString();
        TimeText.text = Time.ToString();
    }
}
