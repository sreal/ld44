﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour {
    public AudioClip Heart;
    public AudioClip Money;
    public AudioClip Gamble;
    public AudioClip Death;

    private AudioSource source;
    void Awake () {
        source = GetComponent<AudioSource>();
    }


    public void PlaySound(CellObject type)
    {
        switch(type) {
        case CellObject.Heart:
            source.PlayOneShot(Heart);
            break;
        case CellObject.Money:
            source.PlayOneShot(Money);
            break;
        case CellObject.Gamble:
            source.PlayOneShot(Gamble);
            break;
        case CellObject.Death:
            source.PlayOneShot(Death);
            break;
        }
    }
}
