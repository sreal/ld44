﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public GameGrid grid { get;set; }
    public RendererManager _renderer;
    public ScoreManager _scorer;

    public AudioManager _player;
    private Constants _const;

    private int playerPosition;
    private bool mustReset = false;

    public Vector3 position = Vector3.one;

    void Awake () {
        _const = new Constants();
        Reset();
    }

    void Reset(bool hard = false) {
        mustReset = false;
        _scorer.Reset(hard);

        if (_scorer.IsOutOfTime()) {
            return;
        }

        var p = (int) (_const.Grid_Height/2);
        playerPosition = p;
        grid = new GameGrid(_const.Grid_Height, _const.Grid_Width);

        _renderer.DrawGameGrid(grid);
        _renderer.DrawPlayer(playerPosition);
    }

    void Update () {
        if (Input.GetKeyUp("escape")) {
            Application.Quit();
            return;
        }
        if (Input.GetKeyUp(KeyCode.R)) {
            Reset(true);
            return;
        }

        if (mustReset) {
            if (Input.GetKeyUp(KeyCode.B)) {
                Reset();
            }
            // exit always with not reset
            return;
        }

        var isDirty = false;
        var isOutOfTime = _scorer.IsOutOfTime();
        if (isOutOfTime && !mustReset) {
            isDirty = true;
            TriggerGameOver();
        } else {
            var direction = Direction.None;
            if (Input.GetKeyUp("up"))
                direction = Direction.Up;
            else if (Input.GetKeyUp("down"))
                direction = Direction.Down;
            else if (Input.GetKeyUp("right"))
                direction = Direction.Right;

            if (direction != Direction.None) {
                isDirty = true;
                UpdatePlayer(direction);
                UpdateScore(playerPosition);
            }
        }

        if (isDirty) {
            isDirty = false;
            _renderer.DrawGameGrid(grid);
            _renderer.DrawPlayer(playerPosition, isOutOfTime);
        }
    }

    void UpdateScore(int height) {
        var type = grid.GetCellObjectAt(0, height);

        _scorer.ProcessMove();
        _scorer.ProcessCell(type);

        _player.PlaySound(type);
        grid.BlankCellObjectAt(0, height);
    }

    void TriggerGameOver() {
        mustReset = true;
        _scorer.GameOver();
        _player.PlaySound(CellObject.Death);
        grid.ZeroCells();
    }

    void UpdatePlayer(Direction direction) {
        switch(direction) {
        case Direction.Up:
            if (playerPosition == grid.Height-1)
                return;
            playerPosition += 1;
            break;
        case Direction.Down:
            if (playerPosition == 0)
                return;
            playerPosition -= 1;
            break;
        case Direction.Right:
            grid.Rotate();
            break;
        }
    }

}
